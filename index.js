//import the express module and call it as a function
const express = require('express');
const app = express();
const PORT = 3000;

app.use(express.static('src'));

//define a route using methods of the Express app object that will correspond to HTTP methods
//Home Page
/*
app.get('/', function(req, res) {
    res.send('Nodemon working');
  });
  // About Page
  app.get('/about', function(req, res) {
    res.send('About Page');
  })
  // Blog Page
  app.get('/blog', function(req, res) {
    res.send('Blog Page');
  })

  // 404 page
app.use(function (req, res, next) {
    res.status(404).send("Oh Oh, you've reached a page not found")
  })
  
//run our application on port 3000, send a message
//that our app is running

*/

app.listen(PORT, function() {
    console.log(`Server listening on port: ${PORT}`);
});

