export default class FIELD extends HTMLElement {

    constructor() {

        super();

        this.attachShadow({mode: 'open'});

        // namespaces
        this.inputId = null;
        this.inputName = null;

        this.sdLabel = null;
        this.sdInput = null;
    }    

    get style() {
        return `
            <style>
                :host {
                    display: block;
                }
                :host * {
                    box-sizing: border-box;
                }
            </style>
        `;
    };

    get template() {
        return `
            <div class="field-container">
                <div class="field-label-container">
                    <slot name="fieldLabel">
                        <span>Label Not Defined</span>
                    </slot>
                </div>
                <div class="field-input-container">
                    <slot name="fieldInput">
                        <span>Input Not Defined</span>
                    </slot>
                </div>
            </div>
        `;
    };

    static get observedAttributes() {
        return ['id', 'name'];
    }

    connectedCallback() {

        this.render();
    }

    attributeChangedCallback(sAttr, sOldValue, sNewValue) {

        console.log("attributeChange");

        // make sure it is mounted first
        if (this.isConnected) {

          switch (sAttr) {

            case 'id':
                this.sInputId = `input-${sNewValue}` || 'no-id';
                break;

            case 'name':
                this.sInputName = sNewValue || 'no-name';
                break;
          }
      
          this.render()
        }
    }

    render() {

        this.shadowRoot.innerHTML = `${this.style}${this.template}`;

        if (!this.sInputName && this.sInputId) {
            this.sInputName = this.getAttribute('id');
        }

        this.sdLabel = this.querySelector('label');
        this.sdInput = this.querySelector('input');

        this.sdLabel.setAttribute('for', this.sInputId);

        this.sdInput.setAttribute('name', this.sInputName);
        this.sdInput.setAttribute('id', this.sInputId)

    }

}

customElements.define('my-field', FIELD);